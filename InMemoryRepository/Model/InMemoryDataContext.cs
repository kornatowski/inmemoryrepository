﻿using InMemoryRepository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace InMemoryRepository.Model
{
    public class InMemoryDataContext<T> : IDataContext<T> where T : IStoreable
    {
        public List<T> Data { get; set; }

        public InMemoryDataContext()
        {
            Data = new List<T>();
        }
    }
}
