﻿using InMemoryRepository.Interface;
using System;

namespace InMemoryRepository.Model
{
    public class Storeable : IStoreable
    {
        public IComparable Id { get; set; }
    }
}
