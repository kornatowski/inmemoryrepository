using InMemoryRepository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InMemoryRepository.Provider
{
    public  class InMemoryRepository<T> : IRepository<T> where T : IStoreable
    {
        private readonly IDataContext<T> _memoryDataContext;

        public InMemoryRepository(IDataContext<T> dataContext)
        {
            _memoryDataContext = dataContext;
        }

        public IEnumerable<T> All()
        {
            return _memoryDataContext.Data;
        }

        public void Delete(IComparable id)
        {
            ValidateId(id);
            _memoryDataContext.Data.RemoveAll(item => item.Id.Equals(id));
        }

        public T FindById(IComparable id)
        {
            ValidateId(id);
            return _memoryDataContext.Data.Find(item => item.Id.Equals(id));
        }

        public void Save(T item)
        {
            ValidateId(item.Id);
            Delete(item.Id);
            _memoryDataContext.Data.Add(item);
        }

        private void ValidateId(IComparable id)
        {
            if (id == null)
                throw new ArgumentNullException("id", "Parameter must be initialized before performing operation.");
        }
    }
}