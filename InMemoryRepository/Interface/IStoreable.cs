﻿using System;

namespace InMemoryRepository.Interface
{
    public interface IStoreable
    {
        IComparable Id { get; set; }
    }
}