using System.Collections.Generic;

namespace InMemoryRepository.Interface
{
    public interface IDataContext<T> where T : IStoreable
    {
        List<T> Data { get; set; }
    }
}