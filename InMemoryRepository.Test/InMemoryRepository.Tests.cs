using System;
using NUnit.Framework;
using FluentAssertions;
using System.Linq;
using System.Collections;
using InMemoryRepository.Test.Model;
using InMemoryRepository.Interface;
using InMemoryRepository.Provider;
using InMemoryRepository.Model;

namespace InMemoryRepository.Test
{
    [TestFixture]
    public class InMemoryRepositoryTests
    {
        private IRepository<TestStoreable> _repository;
        private TestStoreable _storebleObjectOne;
        private TestStoreable _storebleObjectTwo;
        private TestStoreable _storebleObjectThree;
        private TestStoreable _storebleObjectFour;
        private TestStoreable _storebleObjectDuplicateId;

        [SetUp]
        public void SetupInMemoryRepositoryInstance()
        {
            _repository = new InMemoryRepository<TestStoreable>(new InMemoryDataContext<TestStoreable>());
            _storebleObjectOne = new TestStoreable { Id = 1, Value = "Test Value One" };
            _storebleObjectTwo = new TestStoreable { Id = 2, Value = "Test Value Two" };
            _storebleObjectThree = new TestStoreable { Id = 3, Value = "Test Value Three" };
            _storebleObjectFour = new TestStoreable { Id = 4, Value = "Test Value Four" };
            _storebleObjectDuplicateId = new TestStoreable { Id = 1, Value = "Test Value Duplicate Id" };
        }
        
        [Test]
        public void SaveObjectsToEmptyRepository()
        {
            _repository.Save(_storebleObjectOne);
            _repository.Save(_storebleObjectTwo);
            _repository.Save(_storebleObjectThree);
            _repository.Save(_storebleObjectFour);

            _repository.All().Count().Should().Equals(4);

            _repository.All().Should().Contain(_storebleObjectOne);
            _repository.All().Should().Contain(_storebleObjectTwo);
            _repository.All().Should().Contain(_storebleObjectThree);
            _repository.All().Should().Contain(_storebleObjectFour);
        }

        [Test]
        public void SaveAndReplaceExistingObject()
        {
            _repository.Save(_storebleObjectOne);
            _repository.Save(_storebleObjectDuplicateId);

            _repository.FindById(_storebleObjectOne.Id).Value.Should().Be(_storebleObjectDuplicateId.Value);
        }

        [Test]
        public void FindByIdReturnsNull()
        {
            _repository.Save(_storebleObjectOne);
            _repository.Save(_storebleObjectTwo);
            _repository.Save(_storebleObjectThree);

            _repository.FindById(_storebleObjectFour.Id).Should().BeNull();
        }

        [Test]
        public void FindByIdReturnsCorrectObject()
        {
            _repository.Save(_storebleObjectOne);
            _repository.Save(_storebleObjectTwo);
            _repository.Save(_storebleObjectThree);
            _repository.Save(_storebleObjectFour);

            _repository.FindById(_storebleObjectThree.Id).Value.Should().NotBe(_storebleObjectFour.Value);
            _repository.FindById(_storebleObjectThree.Id).Value.Should().Be(_storebleObjectThree.Value);
        }

        [Test]
        public void DeleteRemovesCorrectObject()
        {
            _repository.Save(_storebleObjectOne);
            _repository.Save(_storebleObjectTwo);
            _repository.Save(_storebleObjectThree);
            _repository.Save(_storebleObjectFour);

            _repository.All().Count().Should().Equals(4);

            _repository.Delete(_storebleObjectThree.Id);

            _repository.All().Count().Should().Equals(3);

            _repository.FindById(_storebleObjectThree.Id).Should().BeNull();
        }

        [Test]
        public void DeleteNotExistingObject()
        {
            Action delete = () => _repository.Delete(_storebleObjectOne.Id);
            delete.Should().NotThrow();
        }

        [Test]
        public void ThrowNullReferenceException()
        {
            TestStoreable storeableObjectNull = null;

            Action save = () => _repository.Save(storeableObjectNull);
            save.Should().Throw<NullReferenceException>();
        }

        [Test]
        public void InvalidIdThrowArgumentNullException()
        {
            var storebleObjectNullId = new TestStoreable { Value = "Test Value Null Id" };

            Action save = () => _repository.Save(storebleObjectNullId);
            save.Should().Throw<ArgumentNullException>().And.ParamName.Should().Be("id");

            Action delete = () => _repository.Delete(storebleObjectNullId.Id);
            delete.Should().Throw<ArgumentNullException>().And.ParamName.Should().Be("id");

            Action findById = () => _repository.FindById(storebleObjectNullId.Id);
            findById.Should().Throw<ArgumentNullException>().And.ParamName.Should().Be("id");
        }
    }
}
