﻿using InMemoryRepository.Model;

namespace InMemoryRepository.Test.Model
{
    class TestStoreable : Storeable
    {
        public string Value { get; set; }
    }
}
